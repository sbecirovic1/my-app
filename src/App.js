import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Šeila Bećirović | ARM Test
        </p>
        <a
          className="App-link"
          href="https://seilabecirovic.github.io"
          target="_blank"
          rel="noopener noreferrer"
        >
          Website
        </a>
      </header>
    </div>
  );
}

export default App;
